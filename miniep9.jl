function multiplica(a, b)
	dima = size(a)
	dimb = size(b)
	if dima[2] != dimb[1]
		return -1
	end
	c = zeros(dima[1], dimb[2])
	for i in 1:dima[1]
		for j in 1:dimb[2]
			for k in 1:dima[2]
				c[i, j] = c[i, j] + a[i, k] * b[k, j]
			end
		end
	end
	return c
end

function matrix_pot(m, p)
	matrix = m
	for i in 2:p
		matrix = multiplica(matrix, m)
	end
	return matrix
end

function matrix_pot_by_squaring(m, p)
	if (p == 1)
		return m
	elseif (p % 2 == 0)
		return matrix_pot(multiplica(m, m), p / 2)
	else
		return multiplica(m, matrix_pot(multiplica(m, m), (p - 1) / 2))
	end
end

function test_matrix_pot()
	if (matrix_pot([1 2 ; 3 4], 1) != [ 1 2 ; 3 4 ])
		print("Teste falhou matrix_pot")
	end
	if (matrix_pot([1 2 ; 3 4], 2) != [ 7 10 ; 15 22 ])
		print("Teste falhou matrix_pot")
	end
end

function test_matrix_pot_by_squaring()
	if (matrix_pot_by_squaring([1 2 ; 3 4], 1) != [ 1 2 ; 3 4 ])
		print("Teste falhou matrix_pot_by_squaring")
	end
	if (matrix_pot_by_squaring([1 2 ; 3 4], 2) != [ 7 10 ; 15 22 ])
		print("Teste falhou matrix_pot_by_squaring")
	end
end

using LinearAlgebra
function compare_times()
	M = Matrix(LinearAlgebra.I, 30, 30)
	@time matrix_pot(M, 10)
	@time matrix_pot_by_squaring(M, 10)
end
